package com.projet.letsplay.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.letsplay.model.Publisher;

@Repository
public interface PublisherDAO extends JpaRepository<Publisher, Integer> {
	
	Publisher findById(int id);
	Publisher findByName(String search);
	Boolean existsPublisherByName(String name);

}
