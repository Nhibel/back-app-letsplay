package com.projet.letsplay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.letsplay.model.Studio;

@Repository
public interface StudioDAO extends JpaRepository<Studio, Integer>{
	Studio findById(int id);
	Studio findByName(String search);
	Boolean existsStudioByName(String name);

}
