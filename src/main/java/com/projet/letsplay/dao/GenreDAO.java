package com.projet.letsplay.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.letsplay.model.Genre;

@Repository
public interface GenreDAO extends JpaRepository<Genre, Integer> {
	
	List<Genre> findAllByName(String search);
	Genre findByName(String search);
	Boolean existsGenreByName(String name);
}
