package com.projet.letsplay.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projet.letsplay.model.Profile;


@Repository
public interface ProfileDAO extends JpaRepository<Profile, Integer> {

}
