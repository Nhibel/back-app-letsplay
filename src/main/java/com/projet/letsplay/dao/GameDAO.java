package com.projet.letsplay.dao;

import com.projet.letsplay.model.Game;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameDAO extends JpaRepository<Game, Integer> {

	Game findById(int id);

	List<Game> findByTitleLike(String search);
}
