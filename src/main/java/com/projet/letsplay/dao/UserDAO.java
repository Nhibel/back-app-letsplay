package com.projet.letsplay.dao;

import com.projet.letsplay.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDAO extends JpaRepository<User, Integer> {

	User findById(int id);

	List<User> findByUsernameLike(String search);

}