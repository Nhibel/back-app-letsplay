package com.projet.letsplay.dao;

import java.util.List;
import java.util.Set;

import org.springframework.data.repository.CrudRepository;

import com.projet.letsplay.model.User;

public interface UserFriendDAO extends CrudRepository<User, Long> {

	User findByEmail(String email);
	List<User> findByEmail(Set<String> emails);
}
