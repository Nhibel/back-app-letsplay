package com.projet.letsplay.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projet.letsplay.request.BlockUserRequestEntity;
import com.projet.letsplay.request.SubscribeUserRequestEntity;
import com.projet.letsplay.request.UserFriendsListRequestEntity;
import com.projet.letsplay.request.UserFriendsRequestEntity;
import com.projet.letsplay.service.BlockUserService;
import com.projet.letsplay.service.SubscribeUserService;
import com.projet.letsplay.service.UserFriendsService;

@RestController
@RequestMapping("/api")
public class FriendController {

	@Autowired
	private UserFriendsService userFriendsService;

	@Autowired
	private SubscribeUserService subscribeUserService;

	@Autowired
	private BlockUserService blockUserService;

	/*
	 * { "friends": [ "user1@gmail.com", "user2@gmail.com" ] }
	 */
	@PostMapping(value = "/userFriendRequest")
	public ResponseEntity<Map<String, Object>> userFriendRequest(
			@RequestBody UserFriendsRequestEntity userFriendsRequestEntity) {
		return this.userFriendsService.addUserFriends(userFriendsRequestEntity);
	}

	/*
	 * { "friends": [ "user1@gmail.com", "user2@gmail.com" ] }
	 */
	@PostMapping(value = "/removeUserFriendRequest")
	public ResponseEntity<Map<String, Object>> removeUserFriendRequest(
			@RequestBody UserFriendsRequestEntity userFriendsRequestEntity) {
		return this.userFriendsService.removeUserFriends(userFriendsRequestEntity);
	}

	/*
	 * { "email":shiv.kaushik@gmail.com }
	 */
	@PostMapping(value = "/getUserFriendList")
	public ResponseEntity<Map<String, Object>> getUserFriendList(
			@RequestBody UserFriendsListRequestEntity userFriendsListRequestEntity) {
		return this.userFriendsService.getUserFriendsList(userFriendsListRequestEntity);
	}

	/*
	 * { "friends": [ "meena.verma@gmail.com", "shiv.kaushik@gmail.com" ] }
	 */
	@PostMapping(value = "/getCommonUserFriends")
	public ResponseEntity<Map<String, Object>> getCommonUserFriends(
			@RequestBody UserFriendsRequestEntity userFriendsRequestEntity) {
		return this.userFriendsService.getCommonUserFriends(userFriendsRequestEntity);
	}

	/*
	 * { "requestor":"shiv.kaushik@gmail.com", "target":"ravi.sinde@gmail.com" }
	 */
	@PostMapping(value = "/subscribeUserRequest")
	public ResponseEntity<Map<String, Object>> subscribeUserRequest(
			@RequestBody SubscribeUserRequestEntity subscribeUserRequestEntity) {
		return this.subscribeUserService.addSubscribeUser(subscribeUserRequestEntity);
	}

	/*
	 * { "requestor":"kalpesh.sambre@gmail.com", "target":"ravi.sinde@gmail.com" }
	 */
	@PostMapping(value = "/blockUserRequest")
	public ResponseEntity<Map<String, Object>> blockUserRequest(
			@RequestBody BlockUserRequestEntity blockUserRequestEntity) {
		return this.blockUserService.addBlockUser(blockUserRequestEntity);
	}

}
