package com.projet.letsplay.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.projet.letsplay.dao.GameDAO;
import com.projet.letsplay.dao.ProfileDAO;
import com.projet.letsplay.dao.UserDAO;
import com.projet.letsplay.exceptions.NotFoundException;
import com.projet.letsplay.model.Game;
import com.projet.letsplay.model.Profile;
import com.projet.letsplay.model.User;
import com.projet.letsplay.model.UserSummary;
import com.projet.letsplay.service.UserGamesService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ProfileController {

	@Autowired
	private UserDAO userDao;

	@Autowired
	private ProfileDAO profileDao;

	@Autowired
	private GameDAO gameDao;

	@Autowired
	private UserGamesService userGamesService;

	// ----------------------------------------------------------------------//

	// Return all Users
	/**
	 * Pour récupérer la liste des utilisateurs
	 * @return la liste de tous les utilisateurs présents en BDD
	 * mais ne donne que leur id, leur username, leur date d'inscription et la date de leur dernière connexion
	 * 
	 */
	@GetMapping(value = "/users")
	public MappingJacksonValue userList() {

		List<User> users = userDao.findAll();

		List<UserSummary> userSummaries = new ArrayList<>();

		for (int i = 0; i < users.size(); i++) {
			UserSummary userSummary = new UserSummary(users.get(i).getId(), users.get(i).getUsername(),
					users.get(i).getSignupdate(), users.get(i).getLastconndate());
			userSummaries.add(userSummary);
		}

		MappingJacksonValue userList = new MappingJacksonValue(userSummaries);
		return userList;
	}

	// ----------------------------------------------------------------------//

	// Return User by ID
	/**
	 * Pour retrouver un utilisateur spécifique dans la liste des utilisateurs
	 * 
	 * @param id = id du user que l'on recherche dans la BDD
	 * @return Le user qui correspond à l'id en paramètre
	 */
	@GetMapping(value = "/users/{id}")
	public UserSummary showUser(@PathVariable int id) {
		User user = userDao.findById(id);
		if (user == null)
			throw new NotFoundException("L'user avec l'id " + id + " est introuvable.");
		UserSummary userSummary = new UserSummary(user.getId(), user.getUsername(), user.getSignupdate(),
				user.getLastconndate());
		return userSummary;
	}

	// ----------------------------------------------------------------------//

	// Add User
	/**
	 * Permet d'ajouter un utilisateur à la BDD (pour l'inscription par exemple)
	 * 
	 * @param user = données de l'utilisateur à ajouter à la BDD
	 * @return une réponse http qui confirme ou non l'ajout de l'utilisateur
	 * 
	 *         { "username":"camdev", "password":"4567",
	 *         "email":"camdev@caramail.com", "signupdate":"2019-07-09" }
	 */
	@PostMapping(value = "/users")
	public ResponseEntity<Void> addUser(@Valid @RequestBody User user) {

		User userAdded = userDao.save(user);

		if (userAdded == null) {
			return ResponseEntity.noContent().build();
		}

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(userAdded.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}

	// ----------------------------------------------------------------------//

	// Search by Name
	/**
	 * Permet de trouver un utilisateur grâce à son username ou une partie de son
	 * username
	 * 
	 * @param search = String à comparer aux usernames en BDD
	 * @return les utilisateurs dont le username correspond à la String search
	 */
	@GetMapping(value = "/username/{search}")
	public List<UserSummary> getByName(@PathVariable String search) {
		
		List<User> users =  userDao.findByUsernameLike("%" + search + "%");
		
		List<UserSummary> userSummaries = new ArrayList<>();
		
		for (int i = 0; i < users.size(); i++) {
			UserSummary userSummary = new UserSummary(users.get(i).getId(), users.get(i).getUsername(),
					users.get(i).getSignupdate(), users.get(i).getLastconndate());
			userSummaries.add(userSummary);
		}
		
		return userSummaries;
	}

	// ----------------------------------------------------------------------//

	// Delete User
	/**
	 * Permet de supprimer un utilisateur de la BDD (en cas de désinscription de
	 * l'utilisateur)
	 * 
	 * @param id = id du profil de l'utilisateur à supprimer
	 */
	@DeleteMapping(value = "/users/{id}")
	public void deleteUser(@PathVariable int id) {
		profileDao.deleteById(id);
	}

	// ----------------------------------------------------------------------//

	// Update Profile
	/**
	 * Permet de modifier les informations de profil d'une personne
	 * 
	 * @param id              = id du profil à modifier (table profil)
	 * @param modifiedProfile nouveau profil qui remplacera celui retrouvé dans la
	 *                        BDD
	 * @return le profil modifié
	 */
	@PutMapping(value = "users/{id}/profile")
	public Profile updateProfile(@PathVariable int id, @Valid @RequestBody Profile modifiedProfile) {
		return profileDao.findById(id).map(profil -> {
			profil.setFirstname(modifiedProfile.getFirstname());
			profil.setLastname(modifiedProfile.getLastname());
			profil.setGender(modifiedProfile.getGender());
			profil.setBirthdate(modifiedProfile.getBirthdate());
			profil.setCity(modifiedProfile.getCity());
			profil.setCountry(modifiedProfile.getCountry());
			return profileDao.save(profil);
		}).orElseThrow(() -> new NotFoundException("Le profil n'a pas pu être mis à jour."));

	}

	// ----------------------------------------------------------------------//

	// Update User
	/**
	 * Permet de modifier les informations user d'une personne (email, mot de passe
	 * et date de dernière connexion
	 * 
	 * @param id           id du user à modifier (table user)
	 * @param modifiedUser données du user qui remplaceront les données existantes
	 * @return les données modifiées
	 */
	@PutMapping(value = "users/{id}")
	public User updateUser(@PathVariable int id, @Valid @RequestBody User modifiedUser) {
		User user = userDao.findById(id);
		user.setEmail(modifiedUser.getEmail());
		user.setPassword(modifiedUser.getPassword());
		user.setLastconndate(modifiedUser.getLastconndate());
		userDao.save(user);
		return user;
	}

	// ----------------------------------------------------------------------//
	
	// Ajouter un jeu dans la collection
	/**
	 * Permet d'ajouter un jeu à la bibliothèque d'un utilisateur
	 * @param userId = l'id de l'utilisateur qui veut ajouter le jeu
	 * @param game = le jeu à ajouter à la bibliothèque
	 * @return un message qui confirme ou non l'ajout du jeu à la bibliothèque
	 */
	@PutMapping(value = "users/{userId}/games")
	public String addGames(@PathVariable int userId, @Valid @RequestBody Game game) {
		return userGamesService.addUserGames(userId, game.getId());
	}

	/*
	 * { "id":16 }
	 */
	// Retirer un jeu de la collection
	/**
	 * Permet de retirer un jeu de la bibliothèque d'un utilisateur
	 * @param userId = id de l'utilisateur qui veut supprimer le jeu
	 * @param game = jeu que l'utilisateur veut supprimer
	 * @return un message qui:
	 * - confirme la suppression
	 * - prévient que le jeu n'a pas été trouvé dans la bibliothèque de l'utilisateur
	 */
	@PutMapping(value = "users/{userId}/removeGame")
	public String removeGame(@PathVariable int userId, @Valid @RequestBody Game game) {
		return userGamesService.removeUserGame(userId, game.getId());
	}

	// ----------------------------------------------------------------------//

	// Recherche Simple de joueurs
	//public Sort sortByRelevance(int index) {
	//	return new Sort(Sort.Direction.ASC, "index");
	//}

	// ----------------------------------------------------------------------//

	// Recherche Simple de joueurs
	/**
	 * Retourne une liste de profils qui correspondent aux critères d'une recherche
	 * simple: type de jeu (pour le fun, ...), region et genre de jeux (course, ...)
	 * La liste est triée par pertinence, qui est calculée en fonction des critères
	 * du formulaire Les utilisateurs qui ont le plus de paramètres en commun avec
	 * la recherche arrivent en haut de liste puis ils sont triés par la date de
	 * leur dernière connexion
	 * 
	 * @param profile = les paramètres de recherche entrés par l'utilisateur
	 * @return la liste des utilisateurs, triée en fonction de leur adéquation avec
	 *         la recherche
	 */
//	@GetMapping(value= "users/search")
//	public String[] UserSimpleSearch(@Valid @RequestBody Profile profile) {
//		int indexPertinence = 0; // Le nombre de critères que les utilisateurs et le profil recherché ont en commun
//		List<Profile> profiles = profileDao.findAll();
//		String[][] tabProfiles = new String[profiles.size()][5];
//		
//		for (int i = 0; i< profiles.size(); i++) {
//			tabProfiles[i][0] = profiles.get(i).getUser().getUsername();
//			tabProfiles[i][4] = profiles.get(i).getUser().getLastconndate();
//			
//			if (profile.typeJeu != null) {
//				if (profile.typeJeu.equals(profiles.get(i).typeJeu)) {
//					tabProfiles[i][1] = "OK";
//					indexPertinence++;
//				} else {
//					tabProfiles[i][1] = "";
//				}
//			} else {
//				tabProfiles[i][1] = "OK";
//				indexPertinence++;
//			}
//			
//			if (profile.genre != null) {
//				if (profile.genre.equals(profiles.get(i).genre)) {
//					tabProfiles[i][2] = "OK";
//					indexPertinence++;
//				} else {
//					tabProfiles[i][2] = "";
//				}
//			} else {
//				tabProfiles[i][2] = "OK";
//				indexPertinence++;
//			}
//			
//			if (profile.region != null) {
//				if (profile.region.equals(profiles.get(i).region)) {
//					tabProfiles[i][3] = "OK";
//					indexPertinence++;
//				} else {
//					tabProfiles[i][3] = "";
//				}
//			} else {
//				tabProfiles[i][3] = "OK";
//				indexPertinence++;
//			}
//			tabProfiles[i][4] = Integer.toString(indexPertinence);
//		}
//		return tabProfiles.sortByRelevance("indexPertinence").sortByLastConnDate();
//	}

	// ----------------------------------------------------------------------//

	// Recherche Avancée de joueurs

}
