package com.projet.letsplay.controller;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.projet.letsplay.dao.GameDAO;
import com.projet.letsplay.dao.GenreDAO;
import com.projet.letsplay.dao.PublisherDAO;
import com.projet.letsplay.dao.StudioDAO;
import com.projet.letsplay.exceptions.NotFoundException;
import com.projet.letsplay.model.Game;
import com.projet.letsplay.model.Genre;
import com.projet.letsplay.model.Publisher;
import com.projet.letsplay.model.Studio;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class GameController {

	@Autowired
	private GameDAO gameDao;

	@Autowired
	private GenreDAO genreDao;

	@Autowired
	private PublisherDAO publisherDao;

	@Autowired
	private StudioDAO studioDao;

	// ----------------------------------------------------------------------//

	// Return all Games
	/*
	 * @GetMapping("/games") public MappingJacksonValue gameList() { Iterable<Game>
	 * games = gameDao.findAll(); MappingJacksonValue gameList = new
	 * MappingJacksonValue(games); return gameList; }
	 */

	// Return all Games
	/**
	 * Récupère la liste de tous les jeux présents en BDD
	 * @return la liste des jeux
	 */
	@GetMapping("/games")
	public List<Game> gameList() {
		List<Game> gameList = this.gameDao.findAll();
		return gameList;
	}

	// ---------------------------------------------------------------------- //

	// Return Game by ID
	/**
	 * Retrouve un jeu précis dans la BDD grâce à son id
	 * @param id = l'id du jeu qu'on veut récupérer
	 * @return le jeu qui correspond à l'id passé en paramètre
	 */
	@GetMapping(value = "/games/{id}")
	public Game showGame(@PathVariable int id) {
		Game game = this.gameDao.findById(id);
		if (game == null) {
			throw new NotFoundException("Le jeu avec l'ID " + id + "est introuvable");
		}
		return game;
	}

	// ----------------------------------------------------------------------//

	/*
	 * { "title": "Gran Turismo 14", "description": "test", "pegi": "3", "studio": {
	 * "name": "Sony" }, "publisher": { "name": "Crystal Digital" }, "genre": [ {
	 * "name": "Course" }, { "name": "Simulation" }
	 * 
	 * ] }
	 */
	// Add Game
	/**
	 * Ajoute un jeu en BDD après avoir vérifié qu'il n'existait pas déjà dedans
	 * Vérifie aussi que le(s) genre(s), le publisher et le studio ne sont pas déjà présents en BDD
	 * et les ajoute si ce n'est pas le cas
	 * @param game = le jeu qu'on souhaite ajouter en BDD
	 * @return un message qui:
	 * - confirme l'ajout du jeu à la BDD s'il n'y était pas
	 * - dit que le jeu existe déjà en BDD s'il y est
	 */
	@PostMapping(value = "/games")
	public String addGame(@Valid @RequestBody Game game) {

		String message = null;

		List<Game> games = gameDao.findAll();

		if (games.size() < 1) { // 
			Set<Genre> listGenre = new HashSet<>();
			
			for (Genre genre : game.getGenre()) {
				
				genre = this.genreDao.save(genre);
				listGenre.add(genre);
			}
			game.setGenre(listGenre);

			Publisher gamePub = game.getPublisher();
			gamePub = this.publisherDao.save(gamePub);

			Studio gameStudio = game.getStudio();
			gameStudio = this.studioDao.save(gameStudio);

			gamePub = publisherDao.findByName(gamePub.getName());
			gameStudio = studioDao.findByName(gameStudio.getName());

			game.setGenre(listGenre);
			game.setPublisher(gamePub);
			game.setStudio(gameStudio);

			this.gameDao.save(game);
			message = "Le jeu est ajouté !";

		} else {

			// Vérifie si le jeu est déjà existant en bibliothèque
			for (int i = 0; i < games.size(); i++) {

				String compar = games.get(i).getTitle();

				if (compar.equals(game.getTitle())) {

					message = "Le jeu existe déjà dans la biblio";

					// Si le jeu n'existe pas, on l'ajoute
				} else {
					// Vérifie si le genre existe déjà, si non, l'ajoute.
					Set<Genre> listGenre = new HashSet<>();

					for (Genre genre : game.getGenre()) {
						if (this.genreDao.existsGenreByName(genre.getName())) {
							genre = this.genreDao.findByName(genre.getName());
							listGenre.add(genre);

						} else {
							genre = this.genreDao.save(genre);
							listGenre.add(genre);
						}
					}

					Publisher gamePub = game.getPublisher();
					for (Publisher publisher : publisherDao.findAll()) {

						if (this.publisherDao.existsPublisherByName(gamePub.getName())) {
							gamePub = this.publisherDao.findByName(gamePub.getName());

							System.out.println("dans le if de gamepub" + gamePub.getName());

						} else {
							System.out.println("Dans le else de gamePub");
							gamePub = this.publisherDao.save(gamePub);
						}
					}

					Studio gameStudio = game.getStudio();
					for (Studio studio : studioDao.findAll()) {

						if (this.studioDao.existsStudioByName(gameStudio.getName())) {
							gameStudio = this.studioDao.findByName(gameStudio.getName());

						} else {
							System.out.println("Dans le else de gameStudio");
							gameStudio = this.studioDao.save(gameStudio);
						}
					}

					gamePub = publisherDao.findByName(gamePub.getName());
					gameStudio = studioDao.findByName(gameStudio.getName());

					game.setGenre(listGenre);
					game.setPublisher(gamePub);
					game.setStudio(gameStudio);

					this.gameDao.save(game);
					message = "Le jeu est ajouté !";
				}
			}
		}

		return message;
	}

	// ----------------------------------------------------------------------//

	// Search by Title
	/**
	 * Permet de rechercher un jeu d'après son titre ou une partie de son titre
	 * @param search = chaine de caractères à comparer avec les noms des jeux en BDD
	 * @return la liste des jeux dont le nom contient le paramètre search
	 */
	@GetMapping(value = "/titlegame/{search}")
	public List<Game> getByTitle(@PathVariable String search) {
		return gameDao.findByTitleLike("%" + search + "%");
	}

	// ----------------------------------------------------------------------//

	// Pas de méthode Delete
	// Un user n'a pas le contrôle là-dessus.
}
