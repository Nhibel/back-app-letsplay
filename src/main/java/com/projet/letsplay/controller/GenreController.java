package com.projet.letsplay.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projet.letsplay.dao.GenreDAO;
import com.projet.letsplay.model.Genre;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class GenreController {

	@Autowired
	private GenreDAO genreDao;

	// Rreturn All genre
	@GetMapping(value = "/testgenre1")
	public List<Genre> genreList() {
		List<Genre> genreList = genreDao.findAll();
		return genreList;
	}

	// ---------------------------------------------------------------------- //

	// Ajouter un genre
	@PostMapping(value = "/testgenre3")
	public Genre addGenre(@Valid @RequestBody Genre genre) {

		Genre genreAdded = null;

		Genre genre1 = genreDao.findByName(genre.getName());

		if (genre1 == null) {
			genreAdded = genreDao.save(genre);
		} else {
			genreAdded = genreDao.findByName(genre.getName());
		}
		
		return genreAdded;
	}

	// ----------------------------------------------------------------------//
	
	// Is genre exist ?
	@GetMapping(value = "/testgenre4")
	public Boolean isExist() {
		boolean resultat = genreDao.existsGenreByName("test");
		return resultat;
		
	}
	
	

	// Test Genre 2
//	@GetMapping(value = "/testgenre2")
//	public Genre genreGet() {
//
//		Genre addedGenre = new Genre();
//
//		Genre genre1 = genreDao.findByName("RPG");
//
//		Genre genre2 = new Genre();
//		genre2.setName("test");
//
//		if (genre1.getName().equals(genre2.getName())) {
//			System.out.println("coucou dans le if");
//			addedGenre = genre1;
//		} else {
//			System.out.println("Coucou dans le else");
//			addedGenre = genre2;
//			genreDao.save(addedGenre);
//		}
//
//		return addedGenre;
//	}

	// ---------------------------------------------------------------------- //

}
