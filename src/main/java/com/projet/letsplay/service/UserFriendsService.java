package com.projet.letsplay.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.projet.letsplay.request.UserFriendsListRequestEntity;
import com.projet.letsplay.request.UserFriendsRequestEntity;

public interface UserFriendsService {
	
	ResponseEntity<Map<String, Object>> addUserFriends(UserFriendsRequestEntity userFriendsRequestEntity);
	
	ResponseEntity<Map<String, Object>> removeUserFriends(UserFriendsRequestEntity userFriendsRequestEntity);
	
	ResponseEntity<Map<String, Object>> getUserFriendsList(UserFriendsListRequestEntity userFriendsListRequestEntity);
	
	ResponseEntity<Map<String, Object>> getCommonUserFriends(UserFriendsRequestEntity userFriendsRequestEntity);

}
