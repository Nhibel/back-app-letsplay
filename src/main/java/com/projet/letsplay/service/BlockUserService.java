package com.projet.letsplay.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.projet.letsplay.request.BlockUserRequestEntity;

public interface BlockUserService {
	
	ResponseEntity<Map<String, Object>> addBlockUser(BlockUserRequestEntity blockUserRequestEntity);

}
