package com.projet.letsplay.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projet.letsplay.dao.GameDAO;
import com.projet.letsplay.dao.UserDAO;
import com.projet.letsplay.model.Game;
import com.projet.letsplay.model.User;
import com.projet.letsplay.service.UserGamesService;

@Service
public class UserGamesServiceImpl implements UserGamesService {

	@Autowired
	private GameDAO gameDao;

	@Autowired
	private UserDAO userDao;
	
	/**
	 * Permet d'ajouter un jeu à la bibliothèque d'un utilisateur
	 * @param userId = l'id de l'utilisateur qui veut ajouter le jeu
	 * @param game = le jeu à ajouter à la bibliothèque
	 * @return un message qui confirme ou non l'ajout du jeu à la bibliothèque
	 */
	@Override
	public String addUserGames(int userId, int gameId) {

		User user = userDao.findById(userId);
		Game game = gameDao.findById(gameId);
		String message = "";

		if (user.getUserGames().contains(game)) {
			System.out.println("coucou dans le if");
			message = "Le jeu existe déjà dans la bibliothèque";
		} else {
			System.out.println("coucou dans le else");
			user.getUserGames().add(game);
			this.userDao.save(user);
			message = "Le jeu " + game.getTitle() + " a bien été ajouté à votre profil.";
		}
		return message;
	}

	/**
	 * Permet de retirer un jeu de la bibliothèque d'un utilisateur
	 * @param userId = id de l'utilisateur qui veut supprimer le jeu
	 * @param game = jeu que l'utilisateur veut supprimer
	 * @return un message qui:
	 * - confirme la suppression
	 * - prévient que le jeu n'a pas été trouvé dans la bibliothèque de l'utilisateur
	 */
	public String removeUserGame(int userId, int gameId) {

		User user = userDao.findById(userId);
		Game game = gameDao.findById(gameId);
		String message = "";

		if (user.getUserGames().contains(game)) {

			user.getUserGames().remove(game);
			this.userDao.save(user);
			message = "Le jeu " + game.getTitle() + " a bien été retiré de votre profil.";

		} else {
			message = "Le jeu n'est pas présent dans votre bibliothèque";
		}

		return message;
	}

}
