package com.projet.letsplay.service.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.projet.letsplay.dao.UserFriendDAO;
import com.projet.letsplay.model.User;
import com.projet.letsplay.request.UserFriendsListRequestEntity;
import com.projet.letsplay.request.UserFriendsRequestEntity;
import com.projet.letsplay.service.UserFriendsService;

@Service
public class UserFriendsServiceImpl implements UserFriendsService {
	
	@Autowired
	private UserFriendDAO userFriendDao;
	
	private User saveIfNotExist(String email) {
		
		User existingUser = this.userFriendDao.findByEmail(email);
		if (existingUser == null) {
			existingUser = new User();
			existingUser.setEmail(email);
			return this.userFriendDao.save(existingUser);
		} else {
			return existingUser;
		}
	}

	// Ajout d'un utilisateur à la liste d'ami
	/**
	 * Permet d'ajouter un utilisateur à la liste d'amis
	 * @param userFriendsRequestEntity = un objet composé de deux utilisateurs qui s'ajoutent en amis
	 * @return une réponse HTTP qui confirme ou non l'ajout 
	 */
	@Override
	public ResponseEntity<Map<String, Object>> addUserFriends(UserFriendsRequestEntity userFriendsRequestEntity) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// Si l'objet de la requête est nul
		if (userFriendsRequestEntity == null) {
			result.put("Erreur : ", "requête non valide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		// Si la liste d'ami reçu est vide
		if (CollectionUtils.isEmpty(userFriendsRequestEntity.getFriends())) {
			result.put("Erreur : ", "La liste d'amis ne peut pas être vide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		// Si il manque un mail
		if (userFriendsRequestEntity.getFriends().size() != 2) {
			result.put("Info : ", "Veuillez fournir 2 mails");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		String email1 = userFriendsRequestEntity.getFriends().get(0);
		String email2 = userFriendsRequestEntity.getFriends().get(1);
		
		
		// Si les 2 mails sont similaires
		if (email1.equals(email2)) {
			result.put("Info : ", "Un utilisateur ne peut pas être ami avec lui-même !");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		User user1 = null;
		User user2 = null;
		user1 = this.saveIfNotExist(email1);
		user2 = this.saveIfNotExist(email2);
		
		// Si les utilisateurs sont déjà amis
		if (user1.getUserFriends().contains(user2)) {
			result.put("Info : ", "Impossible d'ajouter, les utilisateurs sont déjà amis");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
		}
		
		// Si l'un des utilisateurs à bloqué l'autre
		if (user1.getBlockUsers().contains(user2)) {
			result.put("Info : ", "Impossible d'ajouter, vous avez bloqué cet utilisateur ou il vous a bloqué");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
		}
		
		user1.addUserFriends(user2);
		this.userFriendDao.save(user1);
		user2.addUserFriends(user1);
		this.userFriendDao.save(user2);
		
		result.put("Success", true);
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}
	
	
		// Suppression d'un utilisateur à la liste d'ami
		@Override
		public ResponseEntity<Map<String, Object>> removeUserFriends(UserFriendsRequestEntity userFriendsRequestEntity) {
			
			Map<String, Object> result = new HashMap<String, Object>();
			
			// Si l'objet de la requête est nul
			if (userFriendsRequestEntity == null) {
				result.put("Erreur : ", "requête non valide");
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
			}
			
			// Si la liste d'ami reçu est vide
			if (CollectionUtils.isEmpty(userFriendsRequestEntity.getFriends())) {
				result.put("Erreur : ", "La liste d'amis ne peut pas être vide");
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
			}
			
			// Si il manque un mail
			if (userFriendsRequestEntity.getFriends().size() != 2) {
				result.put("Info : ", "Veuillez fournir 2 mails");
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
			}
			
			String email1 = userFriendsRequestEntity.getFriends().get(0);
			String email2 = userFriendsRequestEntity.getFriends().get(1);
			
			
			// Si les 2 mails sont similaires
			if (email1.equals(email2)) {
				result.put("Info : ", "Un utilisateur ne peut pas être ami avec lui-même !");
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
			}
			
			User user1 = null;
			User user2 = null;
			user1 = this.saveIfNotExist(email1);
			user2 = this.saveIfNotExist(email2);
			
			// Si les utilisateurs ne sont pas amis
			if (!user1.getUserFriends().contains(user2)) {
				result.put("Info : ", "Impossible de supprimer, les utilisateurs ne sont pas amis");
				return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
			}
			

			user1.removeFriend(user2);
			this.userFriendDao.save(user1);
			result.put("Success", true);
			
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
		}

	// Obtenir la liste d'ami d'un user
	@Override
	public ResponseEntity<Map<String, Object>> getUserFriendsList(UserFriendsListRequestEntity userFriendsListRequestEntity) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// Si la requête est vide
		if (userFriendsListRequestEntity == null) {
			result.put("Erreur : ", "requête non valide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		User user = this.userFriendDao.findByEmail(userFriendsListRequestEntity.getEmail());
		List<String> friendList = user.getUserFriends().stream().map(User::getEmail).collect(Collectors.toList());
		
		result.put("success", true);
		result.put("friends", friendList);
		result.put("count", friendList.size());
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}

	// Voir les amis en communs
	@Override
	public ResponseEntity<Map<String, Object>> getCommonUserFriends(UserFriendsRequestEntity userFriendsRequestEntity) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		// Si la requête est vide
		if (userFriendsRequestEntity == null) {
			result.put("Erreur : ", "Requête invalide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		User user1 = null;
		User user2 = null;
		user1 = this.userFriendDao.findByEmail(userFriendsRequestEntity.getFriends().get(0));
		user2 = this.userFriendDao.findByEmail(userFriendsRequestEntity.getFriends().get(1));
		
		if (user1.getEmail().equals(user2.getEmail())) {
			result.put("Info : ", "Utilisateurs identiques");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		Set<User> friends = null;
		friends = user1.getUserFriends();
		friends.retainAll(user2.getUserFriends());
		
		result.put("Success", true);
		result.put("friends", friends.stream().map(User::getEmail).collect(Collectors.toList()));
		result.put("count", friends.size());
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}
}
