package com.projet.letsplay.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.projet.letsplay.dao.UserFriendDAO;
import com.projet.letsplay.model.User;
import com.projet.letsplay.request.SubscribeUserRequestEntity;
import com.projet.letsplay.service.SubscribeUserService;

@Service
public class SubscribeUserServiceImpl implements SubscribeUserService {
	
	@Autowired
	private UserFriendDAO userFriendDao;

	@Override
	public ResponseEntity<Map<String, Object>> addSubscribeUser(SubscribeUserRequestEntity subscribeUserRequestEntity) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		if (subscribeUserRequestEntity == null) {
			result.put("Erreur : ", "Requête invalide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		if (subscribeUserRequestEntity.getRequestor() == null || subscribeUserRequestEntity.getTarget() == null) {
			result.put("Erreur : ", "Requester or Target ne peuvent pas être vide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		String email1 = subscribeUserRequestEntity.getRequestor();
		String email2 = subscribeUserRequestEntity.getTarget();
		
		if (email1.equals(email2)) {
			result.put("Info : ", "Impossible de s'abonner si les utilisateurs sont les mêmes");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);			
		}
		
		User user1 = this.userFriendDao.findByEmail(email1);
		User user2 = this.userFriendDao.findByEmail(email2);
		
		user1.addSubscribeUsers(user2);
		this.userFriendDao.save(user1);
		
		
		result.put("Success", true);
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}
}
