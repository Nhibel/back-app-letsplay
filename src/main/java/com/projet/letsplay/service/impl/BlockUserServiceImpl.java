package com.projet.letsplay.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.projet.letsplay.dao.UserFriendDAO;
import com.projet.letsplay.model.User;
import com.projet.letsplay.request.BlockUserRequestEntity;
import com.projet.letsplay.service.BlockUserService;

@Service
public class BlockUserServiceImpl implements BlockUserService {
	
	@Autowired
	private UserFriendDAO userFriendDAO;
	
	/**
	 * Permet à un utilisateur d'en bloquer un autre
	 * @param blockUserRequestEntity = un objet requête qui contient un:
	 * - requestor: l'utilisateur qui veut bloquer
	 * - target: l'utilisateur qui doit être bloqué
	 * @return une réponse HTTP qui confirme ou non le blocage
	 */
	@Override
	public ResponseEntity<Map<String, Object>> addBlockUser(BlockUserRequestEntity blockUserRequestEntity) {
		
		Map<String, Object> result = new HashMap<String, Object>();
		
		if (blockUserRequestEntity == null) {
			result.put("Erreur : ", "Requête invalide");
			return new ResponseEntity<Map<String, Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		if (blockUserRequestEntity.getRequestor() == null || blockUserRequestEntity.getTarget() == null) {
			result.put("Erreur : ", "Requestor or Target ne peuvent pas être vide");
			return new ResponseEntity<Map<String,Object>>(result, HttpStatus.BAD_REQUEST);
		}
		
		String email1 = blockUserRequestEntity.getRequestor();
		String email2 = blockUserRequestEntity.getTarget();
		
		if (email1.equals(email2)) {
			result.put("Info : ", "Impossible de se bloquer soi-même");
			return new ResponseEntity<Map<String,Object>>(result, HttpStatus.OK);
		}
		
		User user1 = this.userFriendDAO.findByEmail(email1);
		User user2 = this.userFriendDAO.findByEmail(email2);
		
		user1.addBlockUsers(user2);
		this.userFriendDAO.save(user1);
		
		result.put("Success", true);		
		
		return new ResponseEntity<Map<String, Object>>(result, HttpStatus.OK);
	}

}
