package com.projet.letsplay.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.projet.letsplay.request.SubscribeUserRequestEntity;

public interface SubscribeUserService {
	
	ResponseEntity<Map<String, Object>> addSubscribeUser(SubscribeUserRequestEntity subscribeUserRequestEntity);

}
