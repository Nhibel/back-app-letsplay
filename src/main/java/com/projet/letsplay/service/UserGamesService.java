package com.projet.letsplay.service;

import java.util.Map;

import org.springframework.http.ResponseEntity;

import com.projet.letsplay.request.UserGamesRequestEntity;

public interface UserGamesService {

	String addUserGames(int userId, int gameId);
	String removeUserGame (int userId, int gameId);

}
