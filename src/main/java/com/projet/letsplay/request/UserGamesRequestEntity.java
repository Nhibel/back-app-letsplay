package com.projet.letsplay.request;

import java.util.List;

public class UserGamesRequestEntity {

	private List<String> games;

	public List<String> getGames() {
		return games;
	}

	public void setGames(List<String> games) {
		this.games = games;
	}

}
