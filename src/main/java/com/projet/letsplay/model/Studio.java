// Generated with g9.

package com.projet.letsplay.model;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

@Entity
@Table(name="studio", indexes={@Index(name="studio_id_IX", columnList="id", unique=true)})
public class Studio implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(length=45)
    private String name;
    
    @JsonProperty(access = Access.WRITE_ONLY)
    @OneToMany(mappedBy="studio")
    private Set<Game> game;

    /** Default constructor. */
    public Studio() {
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for name.
     *
     * @return the current value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for name.
     *
     * @param aName the new value for name
     */
    public void setName(String aName) {
        name = aName;
    }

    /**
     * Access method for game.
     *
     * @return the current value of game
     */
    public Set<Game> getGame() {
        return game;
    }

    /**
     * Setter method for game.
     *
     * @param aGame the new value for game
     */
    public void setGame(Set<Game> aGame) {
        game = aGame;
    }

}
