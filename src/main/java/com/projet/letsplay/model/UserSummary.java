package com.projet.letsplay.model;

import java.util.Date;

public class UserSummary {
	
	private int id;
	private String username;
	private Date signupdate;
	private Date lastconndate;
	
	
	
	public UserSummary() {
		super();
	}

	public UserSummary(int id, String username, Date signupdate, Date lastconndate) {
		super();
		this.id = id;
		this.username = username;
		this.signupdate = signupdate;
		this.lastconndate = lastconndate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getSignupdate() {
		return signupdate;
	}

	public void setSignupdate(Date signupdate) {
		this.signupdate = signupdate;
	}

	public Date getLastconndate() {
		return lastconndate;
	}

	public void setLastconndate(Date lastconndate) {
		this.lastconndate = lastconndate;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
