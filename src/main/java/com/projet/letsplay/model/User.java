// Generated with g9.

package com.projet.letsplay.model;

import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.util.CollectionUtils;

@Entity
@Table(name="user", indexes={@Index(name="user_id_IX", columnList="id", unique=true)})
public class User extends AuditModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    @Column(unique=true, nullable=false, precision=10)
    private int id;
	
    @Column(nullable=false, length=45)
    private String username;
    
    @Column(nullable=false, length=45)
    private String password;
    
    @Column(nullable=false, length=45)
    private String email;
    
//    @Column(nullable=false)
//    private Date signupdate;
    
    private Date lastconndate;
    
    @OneToMany(mappedBy="user")
    private Set<Inbox> inbox;
    
    @OneToMany(mappedBy="user2")
    private Set<Inbox> inbox2;
    
    /* Game Management------------------------------------------------------------- */
    
    @ManyToMany
    @JoinTable(name="user_games", joinColumns = @JoinColumn(name="userId"), inverseJoinColumns = @JoinColumn(name= "gameId"))
    private Set<Game> userGames;
    
    /* Friend Management----------------------------------------------------------- */
    
    @ManyToMany
    @JoinTable(name = "user_friends", joinColumns = @JoinColumn(name="userId"), inverseJoinColumns = @JoinColumn(name = "friendId"))
    private Set<User> userFriends;
    
    @ManyToMany
    @JoinTable(name="subscribe_users", joinColumns = @JoinColumn(name = "subscribeUserId"), inverseJoinColumns = @JoinColumn(name="targetUserId"))
    private Set<User> subscribeUsers;
    
    @ManyToMany
    @JoinTable(name = "block_users", joinColumns = @JoinColumn(name = "blockUserId"), inverseJoinColumns = @JoinColumn(name="targetUserId"))
    private Set<User> blockUsers;
    
    // Add friend to User
    /**
     * Méthode qui ajoute un autre utilisateur à la liste d'amis
     * @param user = l'utilisateur qu'on veut ajouter à la liste d'amis de l'utilisateur
     * sur lequel on utilise la méthode
     */
    public void addUserFriends(User user) {
    	if (CollectionUtils.isEmpty(this.subscribeUsers)) {
    		this.subscribeUsers = new HashSet<>();
    	}
    	this.userFriends.add(user);
    }
    
    // Remove friend to User
    /**
     * Permet de retirer un utilisateur de la liste d'amis de l'utilisateur sur lequel
     * la méthode est utilisée
     * @param user = utilisateur à supprimer
     */
    public void removeFriend(User user) {
    	this.userFriends.remove(user);
    }
    
    // Add Subscribe User
    /**
     * Permet d'ajouter un utilisateur à la liste des utilisateurs suivis par celui sur lequel
     * la méthode est utilisée
     * @param user = l'utilisateur à suivre
     */
    public void addSubscribeUsers(User user) {
    	if (CollectionUtils.isEmpty(this.subscribeUsers)) {
    		this.subscribeUsers = new HashSet<>();
    	}
    	this.subscribeUsers.add(user);
    }
    
    // Block user
    /**
     * Permet de bloquer un utilisateur
     * @param user = l'utilisateur à bloquer
     */
    // TOFIX = le blocage a quel effet? 
    public void addBlockUsers(User user) {
    	if (CollectionUtils.isEmpty(this.blockUsers)) {
    		this.blockUsers = new HashSet<>();
    	}
    	this.blockUsers.add(user);
    }
    
    /* ---------------------------------------------------------------------------- */

    /** Default constructor. */
    public User() {
        super();
    }
    
	/**
     * Permet d'obtenir l'id de l'utilisateur
     * @return la valeur de l'attribut id
     */
    public int getId() {
        return id;
    }

    /**
     * Permet de donner une nouvelle valeur à l'id de l'utilisateur
     * @param aId = la valeur que l'on souhaite donner à l'attribut id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Permet d'obtenir le username de l'utilisateur
     * @return la valeur de l'attribut username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Permet de donner une nouvelle valeur au username de l'utilisateur
     * @param aUsername = la valeur que l'on souhaite donner à l'attribut username
     */
    public void setUsername(String aUsername) {
        username = aUsername;
    }

    /**
     * Permet d'obtenir le password de l'utilisateur
     * @return la valeur de l'attribut password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Permet de donner une nouvelle valeur au password de l'utilisateur
     * @param aIPassword = la valeur que l'on souhaite donner à l'attribut password
     */
    public void setPassword(String aPassword) {
        password = aPassword;
    }

    /**
     * Permet d'obtenir l'email de l'utilisateur
     * @return la valeur de l'attribut email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Permet de donner une nouvelle valeur à l'email de l'utilisateur
     * @param aEmail = la valeur que l'on souhaite donner à l'attribut email
     */
    public void setEmail(String aEmail) {
        email = aEmail;
    }

//    /**
//     * Access method for signupdate.
//     *
//     * @return the current value of signupdate
//     */
//    public Date getSignupdate() {
//        return signupdate;
//    }
//
//    /**
//     * Setter method for signupdate.
//     *
//     * @param aSignupdate the new value for signupdate
//     */
//    public void setSignupdate(Date aSignupdate) {
//        signupdate = aSignupdate;
//    }

    /**
     * Permet d'obtenir la date de dernière connexion de l'utilisateur
     * @return la valeur de l'attribut lastconndate
     */
    public Date getLastconndate() {
        return lastconndate;
    }

    /**
     * Permet de d'actualiser la date de dernière connexion de l'utilisateur
     * @param aLastconndate = la valeur que l'on souhaite donner à l'attribut lastconndate
     */
    public void setLastconndate(Date aLastconndate) {
        lastconndate = aLastconndate;
    }

    /**
     * Access method for inbox.
     *
     * @return the current value of inbox
     */
    public Set<Inbox> getInbox() {
        return inbox;
    }

    /**
     * Setter method for inbox.
     *
     * @param aInbox the new value for inbox
     */
    public void setInbox(Set<Inbox> aInbox) {
        inbox = aInbox;
    }

    /**
     * Access method for inbox2.
     *
     * @return the current value of inbox2
     */
    public Set<Inbox> getInbox2() {
        return inbox2;
    }

    /**
     * Setter method for inbox2.
     *
     * @param aInbox2 the new value for inbox2
     */
    public void setInbox2(Set<Inbox> aInbox2) {
        inbox2 = aInbox2;
    }



	/**
	 * @return la liste des amis de l'utilisateur
	 */
	public Set<User> getUserFriends() {
		return userFriends;
	}

	/**
	 * @param userFriends = les amis à ajouter
	 */
	public void setUserFriends(Set<User> userFriends) {
		this.userFriends = userFriends;
	}



	/**
	 * @return la bibliothèque de jeux de l'utilisateur
	 */
	public Set<Game> getUserGames() {
		return userGames;
	}

	/**
	 * @param userGames the userGames to set
	 */
	public void setUserGames(Set<Game> userGames) {
		this.userGames = userGames;
	}

	/**
	 * @return la liste des utilisateurs suivis par l'utilisateur
	 */
	public Set<User> getSubscribeUsers() {
		return subscribeUsers;
	}

	/**
	 * @param subscribeUsers the subscribeUsers to set
	 */
	public void setSubscribeUsers(Set<User> subscribeUsers) {
		this.subscribeUsers = subscribeUsers;
	}

	/**
	 * @return les utilisateurs bloqués par l'utilisateur
	 */
	public Set<User> getBlockUsers() {
		return blockUsers;
	}

	/**
	 * @param blockUsers the blockUsers to set
	 */
	public void setBlockUsers(Set<User> blockUsers) {
		this.blockUsers = blockUsers;
	}
}
