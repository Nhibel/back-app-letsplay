package com.projet.letsplay.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="block_users")
public class BlockUser {
	
	@Id
	@Column
	private int targetUserId;
	
	@ManyToOne
	@JoinColumn(name = "blockUserId", nullable = false)
	private User user;

	/**
	 * @return the targetUserId
	 */
	public int getTargetUserId() {
		return targetUserId;
	}

	/**
	 * @param targetUserId the targetUserId to set
	 */
	public void setTargetUserId(int targetUserId) {
		this.targetUserId = targetUserId;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	

}
