// Generated with g9.

package com.projet.letsplay.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="game", indexes={@Index(name="gamePrimary", columnList="id,fk_idstudio,fk_idpublisher", unique=true)})
public class Game implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    @Column(unique=true, nullable=false, precision=10)
    private int id;
	
    @Column(nullable=false, length=45)
    private String title;
    
    @Column(length=500)
    private String description;
    private Date year;
    
    @Column(length=250)
    private String picture;
    
    @Column(precision=10)
    private int nbPlayer;
    
    @Column(name="PEGI", precision=10)
    private int pegi;
    
//    @OneToMany(mappedBy="game")
//    private Set<GameHasGenre> gameHasGenre;
    
    @ManyToOne(optional=false, fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name="fk_idpublisher", nullable=false)
    private Publisher publisher;

    @ManyToOne(optional=false, fetch = FetchType.EAGER,cascade=CascadeType.ALL)
    @JoinColumn(name="fk_idstudio", nullable=false)
    private Studio studio;
    
//    @OneToMany(mappedBy="game")
//    private Set<UserHasGame> userHasGame;
    
    /* Game Management------------------------------------------------------------- */
    @ManyToMany(mappedBy="userGames")
    @JsonIgnore
    Set<User> games;
    
    /* ----------------------------------------------------------------------------- */
    
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL/*{CascadeType.PERSIST, CascadeType.MERGE}*/)
    @JoinTable(name ="game_has_genre",
    			joinColumns = { @JoinColumn(name = "fk_idgame", referencedColumnName = "id") },
    			inverseJoinColumns = { @JoinColumn(name = "fk_idgenre", referencedColumnName = "id") })
    private Set<Genre> genre;

    /** Default constructor. */
    public Game() {
        super();
    }

    public Set<Genre> getGenre() {
		return genre;
	}

	public void setGenre(Set<Genre> genre) {
		this.genre = genre;
	}
	
	/**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for title.
     *
     * @return the current value of title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Setter method for title.
     *
     * @param aTitle the new value for title
     */
    public void setTitle(String aTitle) {
        title = aTitle;
    }

    /**
     * Access method for description.
     *
     * @return the current value of description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Setter method for description.
     *
     * @param aDescription the new value for description
     */
    public void setDescription(String aDescription) {
        description = aDescription;
    }

    /**
     * Access method for year.
     *
     * @return the current value of year
     */
    public Date getYear() {
        return year;
    }

    /**
     * Setter method for year.
     *
     * @param aYear the new value for year
     */
    public void setYear(Date aYear) {
        year = aYear;
    }

    /**
     * Access method for picture.
     *
     * @return the current value of picture
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Setter method for picture.
     *
     * @param aPicture the new value for picture
     */
    public void setPicture(String aPicture) {
        picture = aPicture;
    }

    /**
     * Access method for nbPlayer.
     *
     * @return the current value of nbPlayer
     */
    public int getNbPlayer() {
        return nbPlayer;
    }

    /**
     * Setter method for nbPlayer.
     *
     * @param aNbPlayer the new value for nbPlayer
     */
    public void setNbPlayer(int aNbPlayer) {
        nbPlayer = aNbPlayer;
    }

    /**
     * Access method for pegi.
     *
     * @return the current value of pegi
     */
    public int getPegi() {
        return pegi;
    }

    /**
     * Setter method for pegi.
     *
     * @param aPegi the new value for pegi
     */
    public void setPegi(int aPegi) {
        pegi = aPegi;
    }

    /**
     * Access method for publisher.
     *
     * @return the current value of publisher
     */
    public Publisher getPublisher() {
        return publisher;
    }

    /**
     * Setter method for publisher.
     *
     * @param aPublisher the new value for publisher
     */
    public void setPublisher(Publisher aPublisher) {
        publisher = aPublisher;
    }

    /**
     * Access method for studio.
     *
     * @return the current value of studio
     */
    public Studio getStudio() {
        return studio;
    }

    /**
     * Setter method for studio.
     *
     * @param aStudio the new value for studio
     */
    public void setStudio(Studio aStudio) {
        studio = aStudio;
    }

//    /**
//     * Access method for userHasGame.
//     *
//     * @return the current value of userHasGame
//     */
//    public Set<UserHasGame> getUserHasGame() {
//        return userHasGame;
//    }
//
//    /**
//     * Setter method for userHasGame.
//     *
//     * @param aUserHasGame the new value for userHasGame
//     */
//    public void setUserHasGame(Set<UserHasGame> aUserHasGame) {
//        userHasGame = aUserHasGame;
//    }

    /**
     * Gets the group fragment id for member studio.
     *
     * @return Current value of the group fragment
     * @see Studio
     */
    public int getStudioId() {
        return (getStudio() == null ? null : getStudio().getId());
    }

    /**
     * Sets the group fragment id from member studio.
     *
     * @param aId New value for the group fragment
     * @see Studio
     */
    public void setStudioId(int aId) {
        if (getStudio() != null) {
            getStudio().setId(aId);
        }
    }

    /**
     * Gets the group fragment id for member publisher.
     *
     * @return Current value of the group fragment
     * @see Publisher
     */
    public int getPublisherId() {
        return (getPublisher() == null ? null : getPublisher().getId());
    }

    /**
     * Sets the group fragment id from member publisher.
     *
     * @param aId New value for the group fragment
     * @see Publisher
     */
    public void setPublisherId(int aId) {
        if (getPublisher() != null) {
            getPublisher().setId(aId);
        }
    }
	/**
	 * @return the games
	 */
    @JsonIgnore
	public Set<User> getGames() {
		return games;
	}
	/**
	 * @param games the games to set
	 */
	public void setGames(Set<User> games) {
		this.games = games;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Game [id=");
		builder.append(id);
		builder.append(", title=");
		builder.append(title);
		builder.append(", description=");
		builder.append(description);
		builder.append(", year=");
		builder.append(year);
		builder.append(", picture=");
		builder.append(picture);
		builder.append(", nbPlayer=");
		builder.append(nbPlayer);
		builder.append(", pegi=");
		builder.append(pegi);
		builder.append(", publisher=");
		builder.append(publisher);
		builder.append(", studio=");
		builder.append(studio);
		builder.append(", games=");
		builder.append(games);
		builder.append(", genre=");
		builder.append(genre);
		builder.append("]");
		return builder.toString();
	}
	
	

}
