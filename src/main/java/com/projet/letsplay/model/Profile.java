// Generated with g9.

package com.projet.letsplay.model;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="profile", indexes={@Index(name="profilePrimary", columnList="id,user_id", unique=true)})
public class Profile implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(length=45)
    private String firstname;
    @Column(length=45)
    private String lastname;
    @Column(precision=3)
    private short gender;
    @Column(nullable=false)
    private Date birthdate;
    @Column(nullable=false, length=45)
    private String country;
    @Column(length=45)
    private String city;
    
    @OneToOne(optional=false)
    @JoinColumn(name="user_id", nullable=false)
    private User user;

    /** Default constructor. */
    public Profile() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for firstname.
     *
     * @return the current value of firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Setter method for firstname.
     *
     * @param aFirstname the new value for firstname
     */
    public void setFirstname(String aFirstname) {
        firstname = aFirstname;
    }

    /**
     * Access method for lastname.
     *
     * @return the current value of lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Setter method for lastname.
     *
     * @param aLastname the new value for lastname
     */
    public void setLastname(String aLastname) {
        lastname = aLastname;
    }

    /**
     * Access method for gender.
     *
     * @return the current value of gender
     */
    public short getGender() {
        return gender;
    }

    /**
     * Setter method for gender.
     *
     * @param aGender the new value for gender
     */
    public void setGender(short aGender) {
        gender = aGender;
    }

    /**
     * Access method for birthdate.
     *
     * @return the current value of birthdate
     */
    public Date getBirthdate() {
        return birthdate;
    }

    /**
     * Setter method for birthdate.
     *
     * @param aBirthdate the new value for birthdate
     */
    public void setBirthdate(Date aBirthdate) {
        birthdate = aBirthdate;
    }

    /**
     * Access method for country.
     *
     * @return the current value of country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Setter method for country.
     *
     * @param aCountry the new value for country
     */
    public void setCountry(String aCountry) {
        country = aCountry;
    }

    /**
     * Access method for city.
     *
     * @return the current value of city
     */
    public String getCity() {
        return city;
    }

    /**
     * Setter method for city.
     *
     * @param aCity the new value for city
     */
    public void setCity(String aCity) {
        city = aCity;
    }

    /**
     * Access method for user.
     *
     * @return the current value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter method for user.
     *
     * @param aUser the new value for user
     */
    public void setUser(User aUser) {
        user = aUser;
    }

    /**
     * Gets the group fragment id for member user.
     *
     * @return Current value of the group fragment
     * @see User
     */
    public int getUserId() {
        return (getUser() == null ? null : getUser().getId());
    }

    /**
     * Sets the group fragment id from member user.
     *
     * @param aId New value for the group fragment
     * @see User
     */
    public void setUserId(int aId) {
        if (getUser() != null) {
            getUser().setId(aId);
        }
    }

}
