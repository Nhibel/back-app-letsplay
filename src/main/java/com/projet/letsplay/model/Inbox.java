// Generated with g9.

package com.projet.letsplay.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="inbox", indexes={@Index(name="inboxPrimary", columnList="id,fk_idsender,fk_idreceiver", unique=true)})
public class Inbox implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)	
    @Column(unique=true, nullable=false, precision=10)
    private int id;
    @Column(nullable=false)
    private Timestamp timestamp;
    @Column(nullable=false, length=1000)
    private String message;
    @ManyToOne(optional=false)
    @JoinColumn(name="fk_idsender", nullable=false)
    private User user;
    @ManyToOne(optional=false)
    @JoinColumn(name="fk_idreceiver", nullable=false)
    private User user2;

    /** Default constructor. */
    public Inbox() {
        super();
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(int aId) {
        id = aId;
    }

    /**
     * Access method for timestamp.
     *
     * @return the current value of timestamp
     */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    /**
     * Setter method for timestamp.
     *
     * @param aTimestamp the new value for timestamp
     */
    public void setTimestamp(Timestamp aTimestamp) {
        timestamp = aTimestamp;
    }

    /**
     * Access method for message.
     *
     * @return the current value of message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Setter method for message.
     *
     * @param aMessage the new value for message
     */
    public void setMessage(String aMessage) {
        message = aMessage;
    }

    /**
     * Access method for user.
     *
     * @return the current value of user
     */
    public User getUser() {
        return user;
    }

    /**
     * Setter method for user.
     *
     * @param aUser the new value for user
     */
    public void setUser(User aUser) {
        user = aUser;
    }

    /**
     * Access method for user2.
     *
     * @return the current value of user2
     */
    public User getUser2() {
        return user2;
    }

    /**
     * Setter method for user2.
     *
     * @param aUser2 the new value for user2
     */
    public void setUser2(User aUser2) {
        user2 = aUser2;
    }

    /**
     * Gets the group fragment id for member user.
     *
     * @return Current value of the group fragment
     * @see User
     */
    public int getUserId() {
        return (getUser() == null ? null : getUser().getId());
    }

    /**
     * Sets the group fragment id from member user.
     *
     * @param aId New value for the group fragment
     * @see User
     */
    public void setUserId(int aId) {
        if (getUser() != null) {
            getUser().setId(aId);
        }
    }

    /**
     * Gets the group fragment id for member user2.
     *
     * @return Current value of the group fragment
     * @see User
     */
    public int getUser2Id() {
        return (getUser2() == null ? null : getUser2().getId());
    }

    /**
     * Sets the group fragment id from member user2.
     *
     * @param aId New value for the group fragment
     * @see User
     */
    public void setUser2Id(int aId) {
        if (getUser2() != null) {
            getUser2().setId(aId);
        }
    }

}
