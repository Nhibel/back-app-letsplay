// Generated with g9.

package com.projet.letsplay.model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "genre", indexes = { @Index(name = "genre_id_IX", columnList = "id", unique = true) })
public class Genre implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int id;

	@Column(length = 45)
	private String name;

//    @OneToMany(mappedBy="genre")
//    private Set<GameHasGenre> gameHasGenre;

	//@ManyToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE }, mappedBy = "genre")
	//private Set<Game> game;

	/** Default constructor. */
	public Genre() {
		super();
	}



//	public Set<Game> getGame() {
//		return game;
//	}
//
//
//
//	public void setGame(Set<Game> game) {
//		this.game = game;
//	}



	/**
	 * Access method for id.
	 *
	 * @return the current value of id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter method for id.
	 *
	 * @param aId the new value for id
	 */
	public void setId(int aId) {
		id = aId;
	}

	/**
	 * Access method for name.
	 *
	 * @return the current value of name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter method for name.
	 *
	 * @param aName the new value for name
	 */
	public void setName(String aName) {
		name = aName;
	}

	/**
	 * Access method for gameHasGenre.
	 *
	 * @return the current value of gameHasGenre
	 */
//    public Set<GameHasGenre> getGameHasGenre() {
//        return gameHasGenre;
//    }

	/**
	 * Setter method for gameHasGenre.
	 *
	 * @param aGameHasGenre the new value for gameHasGenre
	 */
//    public void setGameHasGenre(Set<GameHasGenre> aGameHasGenre) {
//        gameHasGenre = aGameHasGenre;
//    }

}
