package com.projet.letsplay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan({"com.projet.letsplay.dao","com.projet.letsplay.model","com.projet.letsplay.controller", "com.projet.letsplay.service"})
@EntityScan("com.projet.letsplay.model")
@EnableJpaRepositories("com.projet.letsplay.dao")
@EnableJpaAuditing
public class LetsplayApplication {

	public static void main(String[] args) {
		SpringApplication.run(LetsplayApplication.class, args);
	}

}
