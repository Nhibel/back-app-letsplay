-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: dbletsplay
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `game`
--

DROP TABLE IF EXISTS `game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `game` (
  `idgame` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  `picture` varchar(250) DEFAULT NULL,
  `nbPlayer` int(11) DEFAULT NULL,
  `PEGI` int(11) DEFAULT NULL,
  `fk_idstudio` int(11) NOT NULL,
  `fk_idpublisher` int(11) NOT NULL,
  `nb_player` int(11) DEFAULT NULL,
  PRIMARY KEY (`idgame`,`fk_idstudio`,`fk_idpublisher`),
  UNIQUE KEY `gamePrimary` (`idgame`,`fk_idstudio`,`fk_idpublisher`),
  KEY `fk_game_studio1_idx` (`fk_idstudio`),
  KEY `fk_game_publisher1_idx` (`fk_idpublisher`),
  CONSTRAINT `fk_game_publisher1` FOREIGN KEY (`fk_idpublisher`) REFERENCES `publisher` (`idpublish`),
  CONSTRAINT `fk_game_studio1` FOREIGN KEY (`fk_idstudio`) REFERENCES `studio` (`idstudio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game`
--

LOCK TABLES `game` WRITE;
/*!40000 ALTER TABLE `game` DISABLE KEYS */;
/*!40000 ALTER TABLE `game` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_has_genre`
--

DROP TABLE IF EXISTS `game_has_genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `game_has_genre` (
  `fk_idgame` int(11) NOT NULL,
  `fk_idgenre` int(11) NOT NULL,
  PRIMARY KEY (`fk_idgame`,`fk_idgenre`),
  UNIQUE KEY `gameHasGenrePrimary` (`fk_idgame`,`fk_idgenre`),
  KEY `fk_game_has_genre_genre1_idx` (`fk_idgenre`),
  KEY `fk_game_has_genre_game1_idx` (`fk_idgame`),
  CONSTRAINT `fk_game_has_genre_game1` FOREIGN KEY (`fk_idgame`) REFERENCES `game` (`idgame`),
  CONSTRAINT `fk_game_has_genre_genre1` FOREIGN KEY (`fk_idgenre`) REFERENCES `genre` (`idgenre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_has_genre`
--

LOCK TABLES `game_has_genre` WRITE;
/*!40000 ALTER TABLE `game_has_genre` DISABLE KEYS */;
/*!40000 ALTER TABLE `game_has_genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `genre` (
  `idgenre` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idgenre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `genre`
--

LOCK TABLES `genre` WRITE;
/*!40000 ALTER TABLE `genre` DISABLE KEYS */;
/*!40000 ALTER TABLE `genre` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hibernate_sequence`
--

LOCK TABLES `hibernate_sequence` WRITE;
/*!40000 ALTER TABLE `hibernate_sequence` DISABLE KEYS */;
INSERT INTO `hibernate_sequence` VALUES (5);
/*!40000 ALTER TABLE `hibernate_sequence` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inbox`
--

DROP TABLE IF EXISTS `inbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `inbox` (
  `idinbox` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` timestamp NOT NULL,
  `message` varchar(1000) NOT NULL,
  `fk_idsender` int(11) NOT NULL,
  `fk_idreceiver` int(11) NOT NULL,
  PRIMARY KEY (`idinbox`,`fk_idsender`,`fk_idreceiver`),
  UNIQUE KEY `inboxPrimary` (`idinbox`,`fk_idsender`,`fk_idreceiver`),
  KEY `fk_inbox_user1_idx` (`fk_idsender`),
  KEY `fk_inbox_user2_idx` (`fk_idreceiver`),
  CONSTRAINT `fk_inbox_user1` FOREIGN KEY (`fk_idsender`) REFERENCES `user` (`iduser`),
  CONSTRAINT `fk_inbox_user2` FOREIGN KEY (`fk_idreceiver`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inbox`
--

LOCK TABLES `inbox` WRITE;
/*!40000 ALTER TABLE `inbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `inbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `profile` (
  `idprofile` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(45) DEFAULT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `birthdate` date NOT NULL,
  `country` varchar(45) NOT NULL,
  `city` varchar(45) DEFAULT NULL,
  `fk_iduser` int(11) NOT NULL,
  PRIMARY KEY (`idprofile`,`fk_iduser`),
  UNIQUE KEY `profilePrimary` (`idprofile`,`fk_iduser`),
  KEY `fk_profile_user_idx` (`fk_iduser`),
  CONSTRAINT `fk_profile_user` FOREIGN KEY (`fk_iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher`
--

DROP TABLE IF EXISTS `publisher`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `publisher` (
  `idpublish` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idpublish`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher`
--

LOCK TABLES `publisher` WRITE;
/*!40000 ALTER TABLE `publisher` DISABLE KEYS */;
/*!40000 ALTER TABLE `publisher` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `studio`
--

DROP TABLE IF EXISTS `studio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `studio` (
  `idstudio` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idstudio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `studio`
--

LOCK TABLES `studio` WRITE;
/*!40000 ALTER TABLE `studio` DISABLE KEYS */;
/*!40000 ALTER TABLE `studio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `signupdate` date NOT NULL,
  `lastconndate` date DEFAULT NULL,
  PRIMARY KEY (`iduser`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'Nhibel','1234','camilledevos@mail.com','2019-07-01','2019-07-01');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_has_game`
--

DROP TABLE IF EXISTS `user_has_game`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_has_game` (
  `fk_iduser` int(11) NOT NULL,
  `fk_idgame` int(11) NOT NULL,
  PRIMARY KEY (`fk_iduser`,`fk_idgame`),
  UNIQUE KEY `userHasGamePrimary` (`fk_iduser`,`fk_idgame`),
  KEY `fk_user_has_game_game1_idx` (`fk_idgame`),
  KEY `fk_user_has_game_user1_idx` (`fk_iduser`),
  CONSTRAINT `fk_user_has_game_game1` FOREIGN KEY (`fk_idgame`) REFERENCES `game` (`idgame`),
  CONSTRAINT `fk_user_has_game_user1` FOREIGN KEY (`fk_iduser`) REFERENCES `user` (`iduser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_has_game`
--

LOCK TABLES `user_has_game` WRITE;
/*!40000 ALTER TABLE `user_has_game` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_has_game` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-01 22:57:37
